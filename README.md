# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Pulls random wallpaper from Reddit subs and sets wallpaper.
* It picks a random subreddit from the config file, then picks a random image from that to download.
* If for any reason it can't find one (nonexistent subreddit, timeout, etc) it will try again.
* It saves the image in ~/.wallpapers/$(date +%s).jpg (whether or not it's a JPEG file), and then sets it as your wallpaper. 
* Version 0.1

### How do I get set up? ###

* Run wallpaper.sc
* Edit .subreddits to set up Subreddits in use, not including /r/
* Use CRON to set up autoruns
* Place the subreddits you want it to scrape in ~/.subreddits - one per line, without the /r/ part. 
* The defaults are /r/wallpaper, /r/wallpapers, /r/spaceporn, and /r/earthporn.

### Contribution guidelines ###