#!/bin/bash
time=`date +%s`
[[ -d ~/.wallpapers ]] || mkdir ~/.wallpapers
[[ -f ~/.subreddits ]] || echo -e "wallpapers\nwallpaper\nearthporn\nspaceporn" > ~/.subreddits
getWallpaper(){
        echo "getting wallpaper..."
        [[ -z "$1" ]] || echo "Its Not Working!"
        curl -s https://www.reddit.com/r/`shuf -n 1 ~/.subreddits`/.json | python -m json.tool | grep -P '\"url\": \"htt(p|ps):\/\/(i.+)?imgur.com\/(?!.\/)[A-z0-9]{5,7}' | sed -e 's/"url": "//' -e 's/",//' -e 's/gallery\///' -e 's/$/\.jpg/' | shuf -n 1 - | xargs wget -O ~/.wallpapers/$time.jpg
        feh --bg-fill ~/.wallpapers/$time.jpg || getWallpaper "Whoops"
}
getWallpaper
echo "Hope you like your new WallPaper :)"
DIR="/home/michael/.wallpapers"
PIC=$(ls $DIR/* | shuf -n1)
gsettings set org.gnome.desktop.background picture-uri "file://$PIC"
